// Description:
//   Brain of XBot
var nodeConfigs = require('../nodes.json');
var states = require('../states');
var commands = require('../commands');

var cmdList = [{
    name: 'init',
    pattern: /init\s+([^\s]+)/i,
}, {
    name: 'static',
    pattern: /static\s+([^\s]+)/i
}, {
    name: 'start',
    pattern: /start\s+([^\s]+)/i
}, {
    name: 'stop',
    pattern: /stop\s+([^\s]+)/i
}, {
    // TODO should be able to check status for all services
    name: 'status',
    pattern: /status\s+([^\s]+)/i
}, {
    name: 'update',
    pattern: /update\s+([^\s]+)/i
}, {
    // TODO should be able to check config of a specific service
    name: 'show_config',
    pattern: /show config/i
}, {
    // TODO add handler
    name: 'help',
    pattern: /help/i
}, {
    name: 'listServices',
    pattern: /list services/i
}, {
    name: 'yes',
    pattern: /yes/i
}, {
    name: 'no',
    pattern: /no/i
}, {
    name: 'req',
    pattern: /req/i
}];

var findHost = function(name) {
    return nodeConfigs.find(h => h.name === name);
}

module.exports = (robot) => {
    let commonCommandDecorator = function(func) {
        return (cmd, res) => {
            let match = cmd.pattern.exec(res.match[0]);
            let serviceName = match[1];
            let host = findHost(serviceName);

            if (!host) {
                res.send(`can not find service ${serviceName}`)
                return;
            }

            return func(host, cmd, res);
        }
    }

    let execCmd = (target, params, cb) => {
        robot.http(target)
            .header('Accept', 'application/json')
            .header('Content-Type', 'application/json')
            .post(JSON.stringify(params))(cb);
    }

    let init = (host, cmd, res) => {
        let target = `http://${host.host}:${host.port}/init`;

        res.send('initiating...');
        execCmd(target, host.service_config, (err, resp, body) => {
            let msg = JSON.parse(body);
            res.send(msg.data);
        });
    }

    let status = (host, cmd, res) => {
        let target = `http://${host.host}:${host.port}/status`;

        res.send(`getting status for ${host.name}`);
        execCmd(target, host.service_config, (err, resp, body) => {
            let msg = JSON.parse(body);
            res.send(msg.data);
        });
    }

    let start = (host, cmd, res) => {
        let target = `http://${host.host}:${host.port}/start`;

        res.send(`starting ${host.name}`);
        execCmd(target, host.service_config, (err, resp, body) => {
            let msg = JSON.parse(body);
            res.send(msg.data);
        });
    }

    let stop = (host, cmd, res) => {
        let target = `http://${host.host}:${host.port}/stop`;

        res.send(`stoping ${host.name}`);
        execCmd(target, host.service_config, (err, resp, body) => {
            let msg = JSON.parse(body);
            res.send(msg.data);
        });
    }

    let update = (host, cmd, res) => {
        let target = `http://${host.host}:${host.port}/update`;

        res.send(`updating ${host.name}`);
        execCmd(target, host.service_config, (err, resp, body) => {
            let msg = JSON.parse(body);
            res.send(msg.data);
        });
    }

    let static = (host, cmd, res) => {
        let target = `http://${host.host}:${host.port}/run_static`;

        res.send(`compiling static for  ${host.name}`);
        execCmd(target, host.service_config, (err, resp, body) => {
            let msg = JSON.parse(body);
            res.send(msg.data);
        });
    }

    let getUserState = (res) => {
        let userId = res.message.user.id;
        let status = robot.brain.get(userId) || states.START;
        
        return status;
    }

    let setUserState = (res, state) => {
        let userId = res.message.user.id;

        robot.brain.set(userId, state);
    }

    let handlers = {
        init: commonCommandDecorator(init),
        status: commonCommandDecorator(status),
        start: commonCommandDecorator(start),

        stop: commonCommandDecorator(stop),
        update: commonCommandDecorator(update),
        static: commonCommandDecorator(static),
        yes: (cmd, res) => {
            let status = getUserState(res);

            res.send(`yes: ${getUserState(res)}`)
            if (status === states.START) {
                res.send('no idea what you want!');
                return;
            }

            res.send('do it now');
            setUserState(res, states.START);
        },
        no: (cmd, res) => {
            let status = getUserState(res);

            res.send(`no: ${getUserState(res)}`)
            if (status === states.START) {
                res.send('no idea what you want to quit!');
                return;
            }

            res.send('quit not');
            setUserState(res, states.START);
        },
        req: (cmd, res) => {
            setUserState(res, states.INIT);
            res.send(`do you really want to ${res.match[0]}, ${getUserState(res)}`);
        },
        show_config: (cmd, res) => {
            res.send(`config: ${JSON.stringify(nodeConfigs, null, 4)}`);
        }
    };

    robot.respond(/.+/, res => {
        let cmd = cmdList.find(cmd => cmd.pattern.test(res.match[0]));

        if (cmd) {
            let handler = handlers[cmd.name];
            if (handler) {
                handler(cmd, res);
            } else {
                res.send('no handler for this command!');
            }
        } else {
            res.send('pattern not found');
            // TODO process natural language here
        }
    });
}
