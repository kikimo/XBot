# XBot

* XBot(this project)
* A customized [glip hubot adapter](git@github.com:kikimo/hubot-glip.git)
* A bot client: [dclient](git@gitlab.com:kikimo/dclient.git)
* A [test web project](git@gitlab.com:kikimo/testweb.git)
* A deprecated bot: [AAABot](git@gitlab.com:kikimo/AAABot.git)

